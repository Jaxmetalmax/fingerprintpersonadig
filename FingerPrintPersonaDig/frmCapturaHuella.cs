﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DPFP.Gui;

namespace FingerPrintPersonaDig
{
    public partial class frmCapturaHuella : Form
    {
        //Cadena de conexión al servidor de base de datos
        //private const string cCadenaSql = "Data source=DEVLAP\\SQLEXPRESS; initial catalog = TestHuella; user id = devel; password = 123456";

        public frmCapturaHuella()
        {
            InitializeComponent();
        }

        private void enrollmentControl1_OnEnroll(object Control, int FingerMask, DPFP.Template Template, ref DPFP.Gui.EventHandlerStatus EventHandlerStatus)
        {
            var bFp = new byte[1632];//1632 es el tamaño de la huella
            Template.Serialize(ref bFp);//serializa la huella en un array byte[]

            //Inserts Data into SQLite
            Data.insertFPSQLITE(bFp, txtNombre.Text.Trim());
            
            using (SqlConnection oCon = new SqlConnection(Data.cCadenaSql))
            {
                try
                {
                    oCon.Open();
                    string cQuery = "INSERT INTO huellas (nombre, huella) VALUES (@nombre, @huella)";

                    using (var oCom = new SqlCommand(cQuery, oCon))
                    {
                        oCom.Parameters.AddWithValue("@nombre", txtNombre.Text.Trim());
                        //Guardamos en la bdd la huella como un byte[] array (binario)
                        SqlParameter param = oCom.Parameters.Add("@huella", SqlDbType.VarBinary);
                        param.Value = bFp;
                        oCom.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
            }



            EventHandlerStatus = EventHandlerStatus.Success;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmVerificaHuella frm = new frmVerificaHuella();
            frm.ShowDialog();
        }

        private void frmCapturaHuella_Load(object sender, EventArgs e)
        {
            //If not exists create database
            Data.CreateDB();
        }
    }
}
