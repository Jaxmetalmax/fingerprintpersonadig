﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DPFP;

namespace FingerPrintPersonaDig
{
    public partial class frmVerificaHuella : Form
    {
       //Creamos una lista de tipo "Data" donde se guardaran las huellas el id y el nombre
        private List<Data> lHuellas = new List<Data>(); 

        public frmVerificaHuella()
        {
            InitializeComponent();
        }

        private void frmVerificaHuella_Load(object sender, EventArgs e)
        {
            //Descomenta la siguiente linea si quieres cargar las huellas desde SQLite
            //lHuellas=Data.loadFPSQLite();

            
            using (var oCon = new SqlConnection(Data.cCadenaSql))
            {
                try
                {
                    oCon.Open();
                    const string cQuery = "SELECT * FROM huellas ORDER BY id ASC";

                    using (var oCom = new SqlCommand(cQuery, oCon))
                    {
                        SqlDataReader rd = oCom.ExecuteReader();
                        while (rd.Read())
                        {
                            //Para cada registro creamos un memory stream praa la huella
                            using (var mFp = new MemoryStream((byte[])rd["huella"]))
                            {
                                //creamos un objeto de tipo Data para guardar el id, nombre
                                //y huella y lo añadimos a la lista de tipo Data
                                Data datos = new Data();
                                datos.Huella = new Template(mFp);
                                datos.Id = rd["id"].ToString();
                                datos.Nombre = rd["nombre"].ToString();
                                lHuellas.Add(datos);
                            }
                        }
                        oCon.Close();
                    }
                }
                catch (SqlException ex)
                {
                   MessageBox.Show("Error en conexión a la base de datos... " + "ErrCode: " + ex.ErrorCode + ": " + ex.Message);
                }
            }
        }

        private void verificationControl1_OnComplete(object Control, FeatureSet FeatureSet, ref DPFP.Gui.EventHandlerStatus EventHandlerStatus)
        {
            var ver = new DPFP.Verification.Verification();
            var res = new DPFP.Verification.Verification.Result();

            foreach (var datos in lHuellas.Where(datos => datos.Huella != null))
            {
                ver.Verify(FeatureSet, datos.Huella, ref res);
                if (!res.Verified) continue;
                label1.Text = "Verificado... Id: " + datos.Id + " Nombre: " + datos.Nombre;
                break; // success
            }

            if (!res.Verified)
                EventHandlerStatus = DPFP.Gui.EventHandlerStatus.Failure;
        }
    }
}
