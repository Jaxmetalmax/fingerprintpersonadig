﻿using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Windows.Forms;
using DPFP;

namespace FingerPrintPersonaDig
{
    class Data
    {
        public const string cCadenaSql = "Data source=DEVLAP\\SQLEXPRESS; initial catalog = HuellasTest; user id = developer; password = $extr3m3S3CuriTy";
        public string Id { get; set; }
        public string Nombre { get; set; }
        public Template Huella { get; set; }

        public static void CreateDB ()
        {
            if (!File.Exists("test3.db"))
            {
                using (var oCon = new SQLiteConnection("Data Source=test3.db; Version=3;"))
                {
                    const string query = "CREATE TABLE huellas(nombre TEXT(50), huella BLOB)";
                    using (var command = new SQLiteCommand(query, oCon))
                    {
                        oCon.Open();
                        oCon.ChangePassword("SecurePass");
                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        public static void insertFPSQLITE(byte[] bFp, string Nombre)
        {
            if (File.Exists("test3.db"))
            {
                using (var oCon = new SQLiteConnection("Data Source=test3.db; Version=3;"))
                {
                    const string cQuery = "INSERT INTO huellas (nombre, huella) VALUES (@nombre, @huella)";
                    oCon.SetPassword("SecurePass");

                    using (var command = new SQLiteCommand(cQuery, oCon))
                    {
                        oCon.Open();
                        command.Parameters.AddWithValue("@nombre", Nombre);
                        //Guardamos en la bdd la huella como un byte[] array (binario)
                        SQLiteParameter param = command.Parameters.Add("@huella", DbType.Binary);
                        param.Value = bFp;
                        int created = command.ExecuteNonQuery();
                        if (created > 1) MessageBox.Show("Huellas guardadas");
                    }
                }
            }
        }

        public static List<Data> loadFPSQLite()
        {
            var lHuellas = new List<Data>(); 
            if (File.Exists("test3.db"))
            {
                using (var oCon = new SQLiteConnection("Data Source=test3.db; Version=3;"))
                {
                    const string query = "SELECT * FROM huellas";
                    oCon.SetPassword("SecurePass");
                    using (var command = new SQLiteCommand(query, oCon))
                    {
                        oCon.Open();
                        SQLiteDataReader rd = command.ExecuteReader();
                        while (rd.Read())
                        {
                            //Para cada registro creamos un memory stream praa la huella
                            using (var mFp = new MemoryStream((byte[])rd["huella"]))
                            {
                                //creamos un objeto de tipo Data para guardar el id, nombre
                                //y huella y lo añadimos a la lista de tipo Data
                                Data datos = new Data();
                                datos.Huella = new Template(mFp);
                                datos.Id = "1";
                                datos.Nombre = rd["nombre"].ToString();
                                lHuellas.Add(datos);
                            }
                        }
                    }
                }
            }
            return lHuellas;
        }

    }
}
