CREATE DATABASE [HuellasTest]
ON PRIMARY
(NAME = N'HuellasTest',
FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\DATA\HuellasTest.mdf',
SIZE = 2240KB,
MAXSIZE = UNLIMITED,
FILEGROWTH = 1024KB)
LOG ON
(NAME = N'HuellasTest_log',
FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\DATA\HuellasTest_log.ldf',
SIZE = 1024KB,
MAXSIZE = UNLIMITED,
FILEGROWTH = 10%)
COLLATE Modern_Spanish_CI_AS

GO

CREATE TABLE [dbo].[huellas] (
[id] int NOT NULL IDENTITY(1,1) ,
[nombre] varchar(50) NULL ,
[huella] varbinary(MAX) NULL ,
PRIMARY KEY ([id])
)

GO